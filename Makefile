AIRCRAFT=mdanilov-tu-144 a320-neo fgaddon-svn

.PHONY: install repo

install: fgaddon-svn/aircraft.conf
	for i in $(AIRCRAFT); do (cd $$i ; makepkg -fd); done

fgaddon-svn/aircraft.conf: fgaddon-svn/aircraft.conf.template
	cp -f fgaddon-svn/aircraft.conf.template fgaddon-svn/aircraft.conf

repo:
	docker build . -t fgfs-aircraft
	docker run -v `pwd`:/app -w /app -u `id -u`:`id -g` fgfs-aircraft make
